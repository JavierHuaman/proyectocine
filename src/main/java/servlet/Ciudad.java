package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daoimpl.CiudadDaoImpl;
import daoimpl.CiudadDaoImplProxy;

/**
 * Servlet implementation class Ciudad
 */
@WebServlet("/ciudad")
public class Ciudad extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CiudadDaoImpl service = new CiudadDaoImplProxy("http://proyectocinews-jhuaman.rhcloud.com/services/CiudadDaoImpl");


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/json, charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		out.write(service.listar());
		out.close();	
	}

}
