package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.Cliente;
import bean.Reservabutaca;

import com.google.gson.Gson;

import daoimpl.ReservaDaoImpl;
import daoimpl.ReservaDaoImplProxy;

/**
 * Servlet implementation class Reserva
 */
@WebServlet("/reserva")
public class Reserva extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ReservaDaoImpl service = new ReservaDaoImplProxy("http://proyectocinews-jhuaman.rhcloud.com/services/ReservaDaoImpl");

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		
		if(operacion != null){
			if(operacion.equals("agregarBoleto")){
				agregarBoleto(request, response);
			}
			if(operacion.equals("eliminarBoleto")){
				eliminarBoleto(request, response);
			}
			if(operacion.equals("registrar")){
				registrar(request, response);
			}
			if(operacion.equals("buscar")){
				buscar(request, response);
			}
			if(operacion.equals("buscarxCliente")){
				buscarxCliente(request, response);
			}
		}else{
			request.getRequestDispatcher("/cartelera.jsp").forward(request, response);
		}
	}
	
	private void buscarxCliente(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String idCliente = (String) request.getParameter("idCliente");

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		
		out.write(service.buscarPorCliente(Integer.parseInt(idCliente)));
		out.close();	
	}

	private void buscar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
		String idReserva = request.getParameter("idReserva");
		
		response.setContentType("application/json, charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		out.write(service.buscar(Integer.parseInt(idReserva)));
		out.close();	
	}

	@SuppressWarnings("unchecked")
	private void registrar(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String fecha = request.getParameter("fecha");
		int idSala = Integer.parseInt(request.getParameter("idSala"));
		int idCartelera = Integer.parseInt(request.getParameter("idCartelera"));
		HttpSession session = request.getSession();
		
		List<Reservabutaca> reservaButaca = (List<Reservabutaca>) session.getAttribute("listaBoletos");
		Cliente cliente = (Cliente) session.getAttribute("login");
		
		for (Reservabutaca rb : reservaButaca) {
			rb.setIdSala(idSala);
		}
		
		bean.Reserva bean =  new bean.Reserva();
		bean.setIdCartelera(idCartelera);
		bean.setFechaReserva(fecha);
		bean.setEstado("Vigente");
		bean.setReservabutacas(reservaButaca);
		bean.setIdCliente(cliente.getIdCliente());
		Gson gson = new Gson();
		
		bean = gson.fromJson(service.registrar(gson.toJson(bean)), bean.Reserva.class);
		
		
		request.setAttribute("idReserva", bean.getIdReserva());
		request.getRequestDispatcher("/reserva-confirmacion.jsp").forward(request, response);
	}

	@SuppressWarnings("unchecked")
	protected void agregarBoleto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String boleto = request.getParameter("butaca");
		double precio = Double.parseDouble(request.getParameter("precio"));
		HttpSession session = request.getSession();
		List<Reservabutaca> boletos = null;
		if(session.getAttribute("listaBoletos") != null){
			boletos = (List<Reservabutaca>) session.getAttribute("listaBoletos");
		}else{
			boletos = new ArrayList<Reservabutaca>();
		}
		Reservabutaca b = new Reservabutaca();
		b.setButaca(boleto);
		b.setPrecio(precio);
		
		boletos.add(b);

		session.setAttribute("listaBoletos", boletos);
		
	}
	
	@SuppressWarnings("unchecked")
	protected void eliminarBoleto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String boleto = request.getParameter("butaca");
		HttpSession session = request.getSession();
		List<Reservabutaca> boletos = (List<Reservabutaca>) session.getAttribute("listaBoletos");
		
		for (int i = 0; i < boletos.size(); i++) {
			if(boletos.get(i).getButaca().equals(boleto)){
				boletos.remove(boletos.get(i));
				break;
			}
		}
		session.setAttribute("listaBoletos", boletos);
	}

}
