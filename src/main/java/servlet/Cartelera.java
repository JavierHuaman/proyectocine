package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import daoimpl.CarteleraDaoImpl;
import daoimpl.CarteleraDaoImplProxy;
import daoimpl.ReservaButacaDaoImpl;
import daoimpl.ReservaButacaDaoImplProxy;

/**
 * Servlet implementation class Cartelera
 */
@WebServlet("/cartelera")
public class Cartelera extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	CarteleraDaoImpl service = new CarteleraDaoImplProxy("http://proyectocinews-jhuaman.rhcloud.com/services/CarteleraDaoImpl");
	ReservaButacaDaoImpl serviceButaca = new ReservaButacaDaoImplProxy("http://proyectocinews-jhuaman.rhcloud.com/services/ReservaButacaDaoImpl");
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		service(request, response);
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		if(operacion != null){
			if(operacion.equals("listado")){
				listar(request, response);
			}
			if(operacion.equals("traer")){
				traer(request, response);
			}
			if(operacion.equals("cargaButacas")){
				cargarButacas(request, response);
			}
		}else{
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
	}
	
	private void cargarButacas(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession();
		session.removeAttribute("listaBoletos");
		request.getRequestDispatcher("/butacas.jsp").forward(request, response);
	}

	protected void listar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		int complejo = Integer.parseInt(request.getParameter("complejo")); 
		String fecha = request.getParameter("fecha");
		PrintWriter out = response.getWriter();
		out.write(service.listar(complejo, fecha));
		out.close();	
	}
	
	protected void traer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		int cartelera = Integer.parseInt(request.getParameter("cartelera")); 
		String cartelera_dto = service.buscar(cartelera);
		bean.Cartelera car = new Gson().fromJson(cartelera_dto, bean.Cartelera.class);
		String butacas_dto = serviceButaca.verificaReservaButacas(car.getFecha(), car.getIdSala(), car.getHoraInicio());
		StringBuilder builder = new StringBuilder();
		builder.append("{\"cartelera\": ");
		builder.append(cartelera_dto);
		builder.append(", \"butacas\": ");
		builder.append(butacas_dto);
		builder.append("}");
		PrintWriter out = response.getWriter();
		out.write(builder.toString());
	}
}
