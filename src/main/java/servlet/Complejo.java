package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daoimpl.ComplejoDaoImpl;
import daoimpl.ComplejoDaoImplProxy;

/**
 * Servlet implementation class Complejo
 */
@WebServlet("/complejo")
public class Complejo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ComplejoDaoImpl service = new ComplejoDaoImplProxy("http://proyectocinews-jhuaman.rhcloud.com/services/ComplejoDaoImpl");

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/json, charset=UTF-8");
		PrintWriter out = response.getWriter();
		String ciudad = request.getParameter("ciudad");
		out.write(service.listarPorCiudad(Integer.parseInt(ciudad)));
		out.close();	
	}

}
