package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.Cliente;

import com.google.gson.Gson;

import daoimpl.ClienteDaoImpl;
import daoimpl.ClienteDaoImplProxy;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ClienteDaoImpl service  = new ClienteDaoImplProxy("http://proyectocinews-jhuaman.rhcloud.com/services/ClienteDaoImpl");
	
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json, charset=UTF-8");
		String correo = request.getParameter("email");
		String password = request.getParameter("password");
		Gson gson = new Gson();
		String bean = service.logear(correo, password) ;
		if(!bean.equals("null")){
			HttpSession session = request.getSession();
			bean.Cliente l = gson.fromJson(bean, Cliente.class);
			session.setAttribute("login", l);
			response.getWriter().write(bean);
		}else{
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		service(req, resp);
	}

}
