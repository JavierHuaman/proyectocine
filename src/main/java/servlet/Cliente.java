package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import daoimpl.ClienteDaoImpl;
import daoimpl.ClienteDaoImplProxy;

/**
 * Servlet implementation class Cliente
 */
@WebServlet("/cliente")
public class Cliente extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ClienteDaoImpl service  = new ClienteDaoImplProxy("http://proyectocinews-jhuaman.rhcloud.com/services/ClienteDaoImpl");
	
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		if(operacion.equals("registrar")){
			registrar(request, response);
		}else if(operacion.equals("modificar")){
			modificar(request, response);
		}
	}
	
	protected void registrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("nombre");
		String dni = request.getParameter("dni");
		String apellidoPaterno = request.getParameter("apePaterno");
		String apellidoMaterno = request.getParameter("apeMaterno");
		String celular = request.getParameter("celular");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		bean.Cliente bean = new bean.Cliente();
		bean.setApellidoMaterno(apellidoMaterno);
		bean.setApellidoPaterno(apellidoPaterno);
		bean.setNombres(nombre);
		bean.setCelular(celular);
		bean.setDni(dni);
		bean.setEmail(email);
		bean.setPassword(password);
		response.setContentType("application/json, charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
				
		service.registrar(gson.toJson(bean));
		
		String result = service.logear(email, password);
		
		bean = gson.fromJson(result, bean.Cliente.class);
		HttpSession session = request.getSession();
		session.setAttribute("login", bean);
		out.write(result);
		out.close();
	}
	
	protected void modificar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("nombre");
		String dni = request.getParameter("dni");
		String apellidoPaterno = request.getParameter("apePaterno");
		String apellidoMaterno = request.getParameter("apeMaterno");
		String celular = request.getParameter("celular");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String idCliente = request.getParameter("idCliente");
		
		bean.Cliente bean = new bean.Cliente();
		bean.setApellidoMaterno(apellidoMaterno);
		bean.setApellidoPaterno(apellidoPaterno);
		bean.setNombres(nombre);
		bean.setCelular(celular);
		bean.setDni(dni);
		bean.setEmail(email);
		bean.setPassword(password);
		bean.setIdCliente(Integer.parseInt(idCliente));

		HttpSession session = request.getSession();
		bean.Cliente login = (bean.Cliente) session.getAttribute("login");
		
		if(password.equals("")){
			bean.setPassword(login.getPassword());
		}		
		System.out.println(new Gson().toJson(bean));
		
		System.out.println(service.actualizar(new Gson().toJson(bean)));
		
		session.setAttribute("login", bean);
	}
}
