/**
 * CarteleraDaoImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public interface CarteleraDaoImplService extends javax.xml.rpc.Service {
    public java.lang.String getCarteleraDaoImplAddress();

    public daoimpl.CarteleraDaoImpl getCarteleraDaoImpl() throws javax.xml.rpc.ServiceException;

    public daoimpl.CarteleraDaoImpl getCarteleraDaoImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
