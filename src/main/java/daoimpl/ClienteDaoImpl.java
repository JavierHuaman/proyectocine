/**
 * ClienteDaoImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public interface ClienteDaoImpl extends java.rmi.Remote {
    public java.lang.String logear(java.lang.String correo, java.lang.String pass) throws java.rmi.RemoteException;
    public java.lang.String registrar(java.lang.String jsonCliente) throws java.rmi.RemoteException;
    public java.lang.String buscar(java.lang.String dni) throws java.rmi.RemoteException;
    public java.lang.String actualizar(java.lang.String jsonCliente) throws java.rmi.RemoteException;
}
