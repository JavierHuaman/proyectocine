/**
 * ReservaButacaDaoImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public interface ReservaButacaDaoImpl extends java.rmi.Remote {
    public java.lang.String verificaReservaButacas(java.lang.String fecha, int idSala, java.lang.String hora) throws java.rmi.RemoteException;
}
