/**
 * ClienteDaoImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public interface ClienteDaoImplService extends javax.xml.rpc.Service {
    public java.lang.String getClienteDaoImplAddress();

    public daoimpl.ClienteDaoImpl getClienteDaoImpl() throws javax.xml.rpc.ServiceException;

    public daoimpl.ClienteDaoImpl getClienteDaoImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
