package daoimpl;

public class ComplejoDaoImplProxy implements daoimpl.ComplejoDaoImpl {
  private String _endpoint = null;
  private daoimpl.ComplejoDaoImpl complejoDaoImpl = null;
  
  public ComplejoDaoImplProxy() {
    _initComplejoDaoImplProxy();
  }
  
  public ComplejoDaoImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initComplejoDaoImplProxy();
  }
  
  private void _initComplejoDaoImplProxy() {
    try {
      complejoDaoImpl = (new daoimpl.ComplejoDaoImplServiceLocator()).getComplejoDaoImpl();
      if (complejoDaoImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)complejoDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)complejoDaoImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (complejoDaoImpl != null)
      ((javax.xml.rpc.Stub)complejoDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public daoimpl.ComplejoDaoImpl getComplejoDaoImpl() {
    if (complejoDaoImpl == null)
      _initComplejoDaoImplProxy();
    return complejoDaoImpl;
  }
  
  public java.lang.String listarPorCiudad(int ciudad) throws java.rmi.RemoteException{
    if (complejoDaoImpl == null)
      _initComplejoDaoImplProxy();
    return complejoDaoImpl.listarPorCiudad(ciudad);
  }
  
  
}