/**
 * ReservaDaoImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public class ReservaDaoImplServiceLocator extends org.apache.axis.client.Service implements daoimpl.ReservaDaoImplService {

    public ReservaDaoImplServiceLocator() {
    }


    public ReservaDaoImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ReservaDaoImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ReservaDaoImpl
    private java.lang.String ReservaDaoImpl_address = "http://proyectocinews-jhuaman.rhcloud.com/services/ReservaDaoImpl";

    public java.lang.String getReservaDaoImplAddress() {
        return ReservaDaoImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ReservaDaoImplWSDDServiceName = "ReservaDaoImpl";

    public java.lang.String getReservaDaoImplWSDDServiceName() {
        return ReservaDaoImplWSDDServiceName;
    }

    public void setReservaDaoImplWSDDServiceName(java.lang.String name) {
        ReservaDaoImplWSDDServiceName = name;
    }

    public daoimpl.ReservaDaoImpl getReservaDaoImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ReservaDaoImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getReservaDaoImpl(endpoint);
    }

    public daoimpl.ReservaDaoImpl getReservaDaoImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            daoimpl.ReservaDaoImplSoapBindingStub _stub = new daoimpl.ReservaDaoImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getReservaDaoImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setReservaDaoImplEndpointAddress(java.lang.String address) {
        ReservaDaoImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (daoimpl.ReservaDaoImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                daoimpl.ReservaDaoImplSoapBindingStub _stub = new daoimpl.ReservaDaoImplSoapBindingStub(new java.net.URL(ReservaDaoImpl_address), this);
                _stub.setPortName(getReservaDaoImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ReservaDaoImpl".equals(inputPortName)) {
            return getReservaDaoImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://daoimpl", "ReservaDaoImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://daoimpl", "ReservaDaoImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ReservaDaoImpl".equals(portName)) {
            setReservaDaoImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
