package daoimpl;

public class ClienteDaoImplProxy implements daoimpl.ClienteDaoImpl {
  private String _endpoint = null;
  private daoimpl.ClienteDaoImpl clienteDaoImpl = null;
  
  public ClienteDaoImplProxy() {
    _initClienteDaoImplProxy();
  }
  
  public ClienteDaoImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initClienteDaoImplProxy();
  }
  
  private void _initClienteDaoImplProxy() {
    try {
      clienteDaoImpl = (new daoimpl.ClienteDaoImplServiceLocator()).getClienteDaoImpl();
      if (clienteDaoImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)clienteDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)clienteDaoImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (clienteDaoImpl != null)
      ((javax.xml.rpc.Stub)clienteDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public daoimpl.ClienteDaoImpl getClienteDaoImpl() {
    if (clienteDaoImpl == null)
      _initClienteDaoImplProxy();
    return clienteDaoImpl;
  }
  
  public java.lang.String logear(java.lang.String correo, java.lang.String pass) throws java.rmi.RemoteException{
    if (clienteDaoImpl == null)
      _initClienteDaoImplProxy();
    return clienteDaoImpl.logear(correo, pass);
  }
  
  public java.lang.String registrar(java.lang.String jsonCliente) throws java.rmi.RemoteException{
    if (clienteDaoImpl == null)
      _initClienteDaoImplProxy();
    return clienteDaoImpl.registrar(jsonCliente);
  }
  
  public java.lang.String buscar(java.lang.String dni) throws java.rmi.RemoteException{
    if (clienteDaoImpl == null)
      _initClienteDaoImplProxy();
    return clienteDaoImpl.buscar(dni);
  }
  
  public java.lang.String actualizar(java.lang.String jsonCliente) throws java.rmi.RemoteException{
    if (clienteDaoImpl == null)
      _initClienteDaoImplProxy();
    return clienteDaoImpl.actualizar(jsonCliente);
  }
  
  
}