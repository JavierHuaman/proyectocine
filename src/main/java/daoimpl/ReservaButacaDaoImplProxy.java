package daoimpl;

public class ReservaButacaDaoImplProxy implements daoimpl.ReservaButacaDaoImpl {
  private String _endpoint = null;
  private daoimpl.ReservaButacaDaoImpl reservaButacaDaoImpl = null;
  
  public ReservaButacaDaoImplProxy() {
    _initReservaButacaDaoImplProxy();
  }
  
  public ReservaButacaDaoImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initReservaButacaDaoImplProxy();
  }
  
  private void _initReservaButacaDaoImplProxy() {
    try {
      reservaButacaDaoImpl = (new daoimpl.ReservaButacaDaoImplServiceLocator()).getReservaButacaDaoImpl();
      if (reservaButacaDaoImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)reservaButacaDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)reservaButacaDaoImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (reservaButacaDaoImpl != null)
      ((javax.xml.rpc.Stub)reservaButacaDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public daoimpl.ReservaButacaDaoImpl getReservaButacaDaoImpl() {
    if (reservaButacaDaoImpl == null)
      _initReservaButacaDaoImplProxy();
    return reservaButacaDaoImpl;
  }
  
  public java.lang.String verificaReservaButacas(java.lang.String fecha, int idSala, java.lang.String hora) throws java.rmi.RemoteException{
    if (reservaButacaDaoImpl == null)
      _initReservaButacaDaoImplProxy();
    return reservaButacaDaoImpl.verificaReservaButacas(fecha, idSala, hora);
  }
  
  
}