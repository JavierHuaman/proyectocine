/**
 * ComplejoDaoImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public interface ComplejoDaoImplService extends javax.xml.rpc.Service {
    public java.lang.String getComplejoDaoImplAddress();

    public daoimpl.ComplejoDaoImpl getComplejoDaoImpl() throws javax.xml.rpc.ServiceException;

    public daoimpl.ComplejoDaoImpl getComplejoDaoImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
