/**
 * ComplejoDaoImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public interface ComplejoDaoImpl extends java.rmi.Remote {
    public java.lang.String listarPorCiudad(int ciudad) throws java.rmi.RemoteException;
}
