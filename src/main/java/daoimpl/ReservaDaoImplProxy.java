package daoimpl;

public class ReservaDaoImplProxy implements daoimpl.ReservaDaoImpl {
  private String _endpoint = null;
  private daoimpl.ReservaDaoImpl reservaDaoImpl = null;
  
  public ReservaDaoImplProxy() {
    _initReservaDaoImplProxy();
  }
  
  public ReservaDaoImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initReservaDaoImplProxy();
  }
  
  private void _initReservaDaoImplProxy() {
    try {
      reservaDaoImpl = (new daoimpl.ReservaDaoImplServiceLocator()).getReservaDaoImpl();
      if (reservaDaoImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)reservaDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)reservaDaoImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (reservaDaoImpl != null)
      ((javax.xml.rpc.Stub)reservaDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public daoimpl.ReservaDaoImpl getReservaDaoImpl() {
    if (reservaDaoImpl == null)
      _initReservaDaoImplProxy();
    return reservaDaoImpl;
  }
  
  public java.lang.String registrar(java.lang.String jsonReserva) throws java.rmi.RemoteException{
    if (reservaDaoImpl == null)
      _initReservaDaoImplProxy();
    return reservaDaoImpl.registrar(jsonReserva);
  }
  
  public java.lang.String buscarPorCliente(int idCliente) throws java.rmi.RemoteException{
    if (reservaDaoImpl == null)
      _initReservaDaoImplProxy();
    return reservaDaoImpl.buscarPorCliente(idCliente);
  }
  
  public java.lang.String buscar(int id) throws java.rmi.RemoteException{
    if (reservaDaoImpl == null)
      _initReservaDaoImplProxy();
    return reservaDaoImpl.buscar(id);
  }
  
  
}