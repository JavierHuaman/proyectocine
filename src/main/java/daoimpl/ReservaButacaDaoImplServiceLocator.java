/**
 * ReservaButacaDaoImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public class ReservaButacaDaoImplServiceLocator extends org.apache.axis.client.Service implements daoimpl.ReservaButacaDaoImplService {

    public ReservaButacaDaoImplServiceLocator() {
    }


    public ReservaButacaDaoImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ReservaButacaDaoImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ReservaButacaDaoImpl
    private java.lang.String ReservaButacaDaoImpl_address = "http://localhost:9090/proyectocinews/services/ReservaButacaDaoImpl";

    public java.lang.String getReservaButacaDaoImplAddress() {
        return ReservaButacaDaoImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ReservaButacaDaoImplWSDDServiceName = "ReservaButacaDaoImpl";

    public java.lang.String getReservaButacaDaoImplWSDDServiceName() {
        return ReservaButacaDaoImplWSDDServiceName;
    }

    public void setReservaButacaDaoImplWSDDServiceName(java.lang.String name) {
        ReservaButacaDaoImplWSDDServiceName = name;
    }

    public daoimpl.ReservaButacaDaoImpl getReservaButacaDaoImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ReservaButacaDaoImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getReservaButacaDaoImpl(endpoint);
    }

    public daoimpl.ReservaButacaDaoImpl getReservaButacaDaoImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            daoimpl.ReservaButacaDaoImplSoapBindingStub _stub = new daoimpl.ReservaButacaDaoImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getReservaButacaDaoImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setReservaButacaDaoImplEndpointAddress(java.lang.String address) {
        ReservaButacaDaoImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (daoimpl.ReservaButacaDaoImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                daoimpl.ReservaButacaDaoImplSoapBindingStub _stub = new daoimpl.ReservaButacaDaoImplSoapBindingStub(new java.net.URL(ReservaButacaDaoImpl_address), this);
                _stub.setPortName(getReservaButacaDaoImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ReservaButacaDaoImpl".equals(inputPortName)) {
            return getReservaButacaDaoImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://daoimpl", "ReservaButacaDaoImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://daoimpl", "ReservaButacaDaoImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ReservaButacaDaoImpl".equals(portName)) {
            setReservaButacaDaoImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
