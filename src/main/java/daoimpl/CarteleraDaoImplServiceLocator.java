/**
 * CarteleraDaoImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public class CarteleraDaoImplServiceLocator extends org.apache.axis.client.Service implements daoimpl.CarteleraDaoImplService {

    public CarteleraDaoImplServiceLocator() {
    }


    public CarteleraDaoImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CarteleraDaoImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CarteleraDaoImpl
    private java.lang.String CarteleraDaoImpl_address = "http://localhost:9090/proyectocinews/services/CarteleraDaoImpl";

    public java.lang.String getCarteleraDaoImplAddress() {
        return CarteleraDaoImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CarteleraDaoImplWSDDServiceName = "CarteleraDaoImpl";

    public java.lang.String getCarteleraDaoImplWSDDServiceName() {
        return CarteleraDaoImplWSDDServiceName;
    }

    public void setCarteleraDaoImplWSDDServiceName(java.lang.String name) {
        CarteleraDaoImplWSDDServiceName = name;
    }

    public daoimpl.CarteleraDaoImpl getCarteleraDaoImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CarteleraDaoImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCarteleraDaoImpl(endpoint);
    }

    public daoimpl.CarteleraDaoImpl getCarteleraDaoImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            daoimpl.CarteleraDaoImplSoapBindingStub _stub = new daoimpl.CarteleraDaoImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getCarteleraDaoImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCarteleraDaoImplEndpointAddress(java.lang.String address) {
        CarteleraDaoImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (daoimpl.CarteleraDaoImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                daoimpl.CarteleraDaoImplSoapBindingStub _stub = new daoimpl.CarteleraDaoImplSoapBindingStub(new java.net.URL(CarteleraDaoImpl_address), this);
                _stub.setPortName(getCarteleraDaoImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CarteleraDaoImpl".equals(inputPortName)) {
            return getCarteleraDaoImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://daoimpl", "CarteleraDaoImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://daoimpl", "CarteleraDaoImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CarteleraDaoImpl".equals(portName)) {
            setCarteleraDaoImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
