package daoimpl;

public class CarteleraDaoImplProxy implements daoimpl.CarteleraDaoImpl {
  private String _endpoint = null;
  private daoimpl.CarteleraDaoImpl carteleraDaoImpl = null;
  
  public CarteleraDaoImplProxy() {
    _initCarteleraDaoImplProxy();
  }
  
  public CarteleraDaoImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initCarteleraDaoImplProxy();
  }
  
  private void _initCarteleraDaoImplProxy() {
    try {
      carteleraDaoImpl = (new daoimpl.CarteleraDaoImplServiceLocator()).getCarteleraDaoImpl();
      if (carteleraDaoImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)carteleraDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)carteleraDaoImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (carteleraDaoImpl != null)
      ((javax.xml.rpc.Stub)carteleraDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public daoimpl.CarteleraDaoImpl getCarteleraDaoImpl() {
    if (carteleraDaoImpl == null)
      _initCarteleraDaoImplProxy();
    return carteleraDaoImpl;
  }
  
  public java.lang.String buscar(int id) throws java.rmi.RemoteException{
    if (carteleraDaoImpl == null)
      _initCarteleraDaoImplProxy();
    return carteleraDaoImpl.buscar(id);
  }
  
  public java.lang.String listar(int complejo, java.lang.String fecha) throws java.rmi.RemoteException{
    if (carteleraDaoImpl == null)
      _initCarteleraDaoImplProxy();
    return carteleraDaoImpl.listar(complejo, fecha);
  }
  
  
}