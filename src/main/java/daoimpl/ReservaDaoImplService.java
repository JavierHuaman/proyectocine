/**
 * ReservaDaoImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public interface ReservaDaoImplService extends javax.xml.rpc.Service {
    public java.lang.String getReservaDaoImplAddress();

    public daoimpl.ReservaDaoImpl getReservaDaoImpl() throws javax.xml.rpc.ServiceException;

    public daoimpl.ReservaDaoImpl getReservaDaoImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
