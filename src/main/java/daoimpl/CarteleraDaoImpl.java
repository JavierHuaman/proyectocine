/**
 * CarteleraDaoImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public interface CarteleraDaoImpl extends java.rmi.Remote {
    public java.lang.String buscar(int id) throws java.rmi.RemoteException;
    public java.lang.String listar(int complejo, java.lang.String fecha) throws java.rmi.RemoteException;
}
