/**
 * ClienteDaoImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public class ClienteDaoImplServiceLocator extends org.apache.axis.client.Service implements daoimpl.ClienteDaoImplService {

    public ClienteDaoImplServiceLocator() {
    }


    public ClienteDaoImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ClienteDaoImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ClienteDaoImpl
    private java.lang.String ClienteDaoImpl_address = "http://localhost:9090/proyectocinews/services/ClienteDaoImpl";

    public java.lang.String getClienteDaoImplAddress() {
        return ClienteDaoImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ClienteDaoImplWSDDServiceName = "ClienteDaoImpl";

    public java.lang.String getClienteDaoImplWSDDServiceName() {
        return ClienteDaoImplWSDDServiceName;
    }

    public void setClienteDaoImplWSDDServiceName(java.lang.String name) {
        ClienteDaoImplWSDDServiceName = name;
    }

    public daoimpl.ClienteDaoImpl getClienteDaoImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ClienteDaoImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getClienteDaoImpl(endpoint);
    }

    public daoimpl.ClienteDaoImpl getClienteDaoImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            daoimpl.ClienteDaoImplSoapBindingStub _stub = new daoimpl.ClienteDaoImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getClienteDaoImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setClienteDaoImplEndpointAddress(java.lang.String address) {
        ClienteDaoImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (daoimpl.ClienteDaoImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                daoimpl.ClienteDaoImplSoapBindingStub _stub = new daoimpl.ClienteDaoImplSoapBindingStub(new java.net.URL(ClienteDaoImpl_address), this);
                _stub.setPortName(getClienteDaoImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ClienteDaoImpl".equals(inputPortName)) {
            return getClienteDaoImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://daoimpl", "ClienteDaoImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://daoimpl", "ClienteDaoImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ClienteDaoImpl".equals(portName)) {
            setClienteDaoImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
