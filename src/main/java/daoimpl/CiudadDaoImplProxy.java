package daoimpl;

public class CiudadDaoImplProxy implements daoimpl.CiudadDaoImpl {
  private String _endpoint = null;
  private daoimpl.CiudadDaoImpl ciudadDaoImpl = null;
  
  public CiudadDaoImplProxy() {
    _initCiudadDaoImplProxy();
  }
  
  public CiudadDaoImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initCiudadDaoImplProxy();
  }
  
  private void _initCiudadDaoImplProxy() {
    try {
      ciudadDaoImpl = (new daoimpl.CiudadDaoImplServiceLocator()).getCiudadDaoImpl();
      if (ciudadDaoImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)ciudadDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)ciudadDaoImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (ciudadDaoImpl != null)
      ((javax.xml.rpc.Stub)ciudadDaoImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public daoimpl.CiudadDaoImpl getCiudadDaoImpl() {
    if (ciudadDaoImpl == null)
      _initCiudadDaoImplProxy();
    return ciudadDaoImpl;
  }
  
  public java.lang.String listar() throws java.rmi.RemoteException{
    if (ciudadDaoImpl == null)
      _initCiudadDaoImplProxy();
    return ciudadDaoImpl.listar();
  }
  
  
}