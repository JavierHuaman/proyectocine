/**
 * ReservaDaoImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public interface ReservaDaoImpl extends java.rmi.Remote {
    public java.lang.String registrar(java.lang.String jsonReserva) throws java.rmi.RemoteException;
    public java.lang.String buscarPorCliente(int idCliente) throws java.rmi.RemoteException;
    public java.lang.String buscar(int id) throws java.rmi.RemoteException;
}
