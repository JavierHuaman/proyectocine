/**
 * ComplejoDaoImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package daoimpl;

public class ComplejoDaoImplServiceLocator extends org.apache.axis.client.Service implements daoimpl.ComplejoDaoImplService {

    public ComplejoDaoImplServiceLocator() {
    }


    public ComplejoDaoImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ComplejoDaoImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ComplejoDaoImpl
    private java.lang.String ComplejoDaoImpl_address = "http://localhost:9090/proyectocinews/services/ComplejoDaoImpl";

    public java.lang.String getComplejoDaoImplAddress() {
        return ComplejoDaoImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ComplejoDaoImplWSDDServiceName = "ComplejoDaoImpl";

    public java.lang.String getComplejoDaoImplWSDDServiceName() {
        return ComplejoDaoImplWSDDServiceName;
    }

    public void setComplejoDaoImplWSDDServiceName(java.lang.String name) {
        ComplejoDaoImplWSDDServiceName = name;
    }

    public daoimpl.ComplejoDaoImpl getComplejoDaoImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ComplejoDaoImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getComplejoDaoImpl(endpoint);
    }

    public daoimpl.ComplejoDaoImpl getComplejoDaoImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            daoimpl.ComplejoDaoImplSoapBindingStub _stub = new daoimpl.ComplejoDaoImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getComplejoDaoImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setComplejoDaoImplEndpointAddress(java.lang.String address) {
        ComplejoDaoImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (daoimpl.ComplejoDaoImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                daoimpl.ComplejoDaoImplSoapBindingStub _stub = new daoimpl.ComplejoDaoImplSoapBindingStub(new java.net.URL(ComplejoDaoImpl_address), this);
                _stub.setPortName(getComplejoDaoImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ComplejoDaoImpl".equals(inputPortName)) {
            return getComplejoDaoImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://daoimpl", "ComplejoDaoImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://daoimpl", "ComplejoDaoImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ComplejoDaoImpl".equals(portName)) {
            setComplejoDaoImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
