<%@page import="bean.Reservabutaca"%>
<%@page import="java.util.List"%>
<%@page import="bean.Cliente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="templates/estilos.jsp" flush="true">
	<jsp:param value="Reserva" name="titulo" />
</jsp:include>
</head>
<body>
	<% Cliente bean = (Cliente) session.getAttribute("login");
	   String idCartelera = request.getParameter("idCartelera");
	   String idSala = request.getParameter("idSala");
	   String fecha = request.getParameter("fecha");
	   List<Reservabutaca> butacas = (List<Reservabutaca>) session.getAttribute("listaBoletos");
	   double total = 0;
	   for(Reservabutaca rb: butacas){
		   total += rb.getPrecio();
	   }
	%>
	<div class="wrapper">
		<jsp:include page="templates/menu.jsp" flush="true"></jsp:include>

		<!-- Main content -->
		<section class="container">
		<div class="order-container">
			<div class="order">
				<img class="order__images" alt='' src="images/tickets.png">
				<p class="order__title">
					Reservando </span>
				</p>
			</div>
		</div>
		<div class="order-step-area">
			<div class="order-step second--step order-step--disable">Eliga
				un asiento</div>
			<div class="order-step third--step">Termine Reserva</div>
		</div>

		<div class="col-sm-12">
			<div class="checkout-wrapper">
				<h2 class="page-heading">Precio</h2>
				<ul class="book-result">
					<li class="book-result__item">Butacas: <span
						class="book-result__count booking-ticket"><%=butacas.size() %></span></li>
					<li class="book-result__item">Total: <span
						class="book-result__count booking-cost">S/. <%=total %></span></li>
				</ul>

				<h2 class="page-heading">Informacion Cliente</h2>

				<form id='contact-info' method='post' class="form contact-info">
					<div class="contact-info__field">
  						<input type="text" class="form__mail" disabled value="<%= bean.getNombres() %> <%=bean.getApellidoPaterno() %>">
                     </div>
					<div class="contact-info__field">
  						<input type="text" class="form__mail" disabled value="<%= bean.getCelular() %>">
                     </div>
					<div class="contact-info__field">
  						<input type="text" class="form__mail" disabled value="<%= bean.getCelular() %>">
                     </div>
					<div class="contact-info__field">
  						<input type="text" class="form__mail" disabled value="<%= bean.getEmail() %>">
                     </div>
				</form>

				<p class="reservation-message">Recibira un mensaje de confirmacion.</p>

			</div>

			<div class="order">
				<form action="reserva" method="post">
					<input type="hidden" name="operacion" value="registrar" />
					<input type="hidden" name="idCartelera" value="<%=idCartelera %>" />
					<input type="hidden" name="idSala" value="<%=idSala %>" />
					<input type="hidden" name="fecha" value="<%=fecha %>" />
					<button class="btn btn-md btn--warning btn--wide">Reservar</button>
				</form>
			</div>
		</div>

		</section>

		<div class="clearfix"></div>

		<jsp:include page="templates/footer.jsp" flush="true"></jsp:include>
	</div>
	<jsp:include page="templates/librerias.jsp" flush="true"></jsp:include>
</body>
</html>