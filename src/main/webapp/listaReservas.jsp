<%@page import="bean.Cliente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="templates/estilos.jsp" flush="true">
	<jsp:param value="Inicio" name="titulo" />
</jsp:include>
<!-- Mono font -->
<link href='http://fonts.googleapis.com/css?family=PT+Mono' rel='stylesheet' type='text/css'>
<!-- Roboto -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
</head>
<body>
	<%
		Cliente bean = (Cliente) session.getAttribute("login");
	%>
	<div class="wrapper">
		<!-- Header section -->
		<jsp:include page="templates/menu.jsp" flush="true"></jsp:include>

		<!-- Main content -->
		<section class="container">
		<h2 class="heading heading--outcontainer heading--top">Reservas</h2>
		<!-- Dark accordion-->
		<div class="col-sm-6">
			<!-- Accordion markup-->
			<div class="panel-group accordion-dark" id="accordion-dark">

			</div>
			<!-- end accordion -->
		</div>

		</section>

		<div class="clearfix"></div>

		<jsp:include page="templates/footer.jsp" flush="true"></jsp:include>
		<div id="test-modal" class="mfp-hide white-popup-block ticket" style="max-width: 100%;height: 100%;max-height: 100%;">
			<div class="ticket-position">
				<div class="ticket__indecator indecator--pre">
					<div class="indecator-text pre--text">Ticket online</div>
				</div>
				<div class="ticket__inner">

					<div class="ticket-secondary">
						<span class="ticket__item">Numero de Ticket <strong
							class="ticket__number"></strong></span> <span
							class="ticket__item ticket__date"></span> <span
							class="ticket__item ticket__time"></span> <span
							class="ticket__item">Local: <span class="ticket__cinema"></span></span>
						<span class="ticket__item">Cliente: <span
							class="ticket__hall"></span></span> <span
							class="ticket__item ticket__price">Precio: <strong
							class="ticket__cost"></strong></span>
					</div>

					<div class="ticket-primery">
						<span class="ticket__item ticket__item--primery ticket__film">Pelicula<br>
							<strong class="ticket__movie"></strong></span> <span
							class="ticket__item ticket__item--primery">Butacas: <span
							class="ticket__place"></span></span>
					</div>


				</div>
				<div class="ticket__indecator indecator--post">
					<div class="indecator-text post--text">Ticket online</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="templates/librerias.jsp" flush="true"></jsp:include>
	<script type="text/javascript">
		$( document ).ajaxStart(function() {
			$.blockUI({ css: { 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
	        } }); 
		});
		$( document ).ajaxStop(function() {
			$.unblockUI();
		});
	    $(document).ready(function() {
	    	var idCliente = "<%=bean.getIdCliente()%>";
			$.getJSON('reserva', {
				operacion : 'buscarxCliente',
				idCliente : idCliente
			}, function(data) {
				var html = [];
				console.log(data.length);
				if (data.length == 0){
					html.push('<h3>No tiene reservas</h3>');
				}
				$.each(data, function(k, v){
					html.push('<div class="panel panel-default">');
					html.push('<div class="panel-heading">');
					html.push('<h4 class="panel-title">');
					html.push('<a data-parent="#accordion-dark"  class="collapsed" style="cursor:pointer;" id-reserva="'+v.idReserva+'">');
					html.push('<span class="marker"><span class="custom1">+</span> </span> ');
					html.push(v.numeroTicket+"  -  "+v.fechaReserva+"  -  "+v.estado);
					html.push('</a></h4></div></div>');
				});
				$('#accordion-dark').html(html.join(''));
				$('.collapsed').click(function(){
					var idReserva = $(this).attr('id-reserva');
					$.getJSON('reserva', {
						operacion : 'buscar',
						idReserva : idReserva
					}, function(data){
						console.log(data);
						$('.ticket__number').html(data.numeroTicket);
						$('.ticket__date').html(data.fechaReserva);
						$('.ticket__time').html(data.cartelera.horaInicio);
						$('.ticket__cinema').html(data.cartelera.sala.complejo.nombre);
						$('.ticket__hall').html(data.cliente.nombres+" "+data.cliente.apellidoPaterno);
						$('.ticket__movie').html(data.cartelera.pelicula.nombre);
						var total = 0;
						var butaca = "";
						$.each(data.reservabutacas, function(k, v){
							total += v.precio;
							butaca += ", "+v.butaca;
						});
						butaca = butaca.substr(2);
						$('.ticket__cost').html("S/. " + total );
						$('.ticket__place').html(butaca);
						$.magnificPopup.open({
	       			        items: {
	       			            src: '#test-modal' 
	       			        },
	       			        type: 'inline'
	       			    });
					});
				});
			});
		});
	</script>

</body>
</html>