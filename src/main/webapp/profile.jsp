<%@page import="bean.Cliente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="templates/estilos.jsp" flush="true">
	<jsp:param value="Perfil" name="titulo" />
</jsp:include>
</head>
<body>
	<% Cliente bean = (Cliente) session.getAttribute("login"); %>
	<div class="wrapper">
		<!-- Header section -->
		<jsp:include page="templates/menu.jsp" flush="true"></jsp:include>

		<div class="clearfix"></div>
		<!-- Main content -->
		<form id="profile-form" class="login" method='get' action="cliente" novalidate=''>
			<p class="login__title">Bienvenido<br>
				<span class="login-edition">a AMovie</span>
			</p>
	
			<div class="field-wrap">
				<label>DNI</label>
				<input class="login__input" type="text" placeholder="DNI" name="dni" value="<%=bean.getDni() %>" disabled="disabled"/>
			</div>
			
			<div class="field-wrap">
				<label>Nombre</label>
				<input class="login__input" type="text" placeholder="Nombre" name="nombre" value="<%=bean.getNombres() %>" disabled="disabled"/>
			</div>
			
			<div class="field-wrap">
				<label>Apellido Paterno</label>
				<input class="login__input" type="text" placeholder="Apellido Paterno" name="apePaterno" value="<%=bean.getApellidoPaterno() %>" disabled="disabled"/>
			</div>
			
			<div class="field-wrap">
				<label>Apellido Materno</label>
				<input class="login__input" type="text" placeholder="Apellido Materno" name="apeMaterno" value="<%=bean.getApellidoMaterno() %>" disabled="disabled"/>
			</div>
			
			<div class="field-wrap">
				<label>Celular</label>
				<input class="login__input" type="text" placeholder="Celular" name="celular" value="<%=bean.getCelular() %>" disabled="disabled"/>
			</div>
			
			<div class="field-wrap">
				<label>Correo</label>
				<input class="login__input" type="email" placeholder="Correo" name="email" value="<%=bean.getEmail() %>" disabled="disabled"/>
			</div>
			
			<div class="field-wrap">
				<label>Password</label>
				<input class="login__input" type="password" placeholder="Password" name="password" disabled="disabled"/>
			</div>
			<input type="hidden" name="is_profile"  value="0"/>>
			<input type="hidden" name="operacion"  value="modificar"/>
			<input type="hidden" name="idCliente" value="<%=bean.getIdCliente() %>" />
			<div class="login__control">
				<button type='submit' class="btn btn-md btn--warning btn--wider" style="margin-top: 20px;" id="btnModificar">Modificar</button>
			</div>
		</form>
	</div>
	<jsp:include page="templates/footer.jsp" flush="true"></jsp:include>
	<jsp:include page="templates/librerias.jsp" flush="true"></jsp:include>
	<script type="text/javascript">
	    $(document).ready(function() {
	        init_Profile();
	    });
	</script>
</body>
</html>