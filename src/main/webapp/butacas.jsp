<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="templates/estilos.jsp" flush="true">
	<jsp:param value="Asientos" name="titulo" />
</jsp:include>
</head>
<body>
	<div class="wrapper">
		<jsp:include page="templates/menu.jsp" flush="true"></jsp:include>
		
		<!-- Main content -->
		<div class="place-form-area">
			<section class="container">
			<div class="order-container">
				<div class="order">
					<img class="order__images" alt='' src="images/tickets.png">
					<p class="order__title">
						Reservando </span>
					</p>
				</div>
			</div>
			<div class="order-step-area">
				<div class="order-step second--step">Eliga un asiento</div>
			</div>
			<h2 class="page-heading heading--outcontainer">Pelicula escogida</h2>
			<section class="container">
			<div class="col-sm-12">
				<div class="choose-indector choose-indector--film hide-content">
					<strong>Nombre: </strong><span class="choosen-area" id="IDPelicula"></span>
				</div>

				<h2 class="page-heading">Complejo &amp; Fecha</h2>

				<ul class="book-result">
                    <li class="book-result__item">Complejo: <span class="book-result__count booking-ticket" id="IDComplejo"></span></li>
                    <li class="book-result__item">Fecha: <span class="book-result__count booking-price" id="IDFecha"></span></li>
                </ul>

				<h2 class="page-heading">Horario escogio</h2>
	
				<div class="choose-indector choose-indector--time hide-content">
					<strong>Hora: </strong><span class="choosen-area" id="IDHorario"></span>
				</div>
			</div>

			</section>
			<div class="clearfix"></div>
			<div class="choose-sits">
				<div class="choose-sits__info choose-sits__info--first">
					<ul>
						<li class="sits-price marker--none"><strong>Precio</strong></li>
						<li class="sits-price sits-price--cheap">S/.10</li>
						<li class="sits-price sits-price--middle">S/.20</li>
						<li class="sits-price sits-price--expensive">S/.30</li>
					</ul>
				</div>

				<div class="choose-sits__info">
					<ul>
						<li class="sits-state sits-state--not">No disponible</li>
						<li class="sits-state sits-state--your">Seleccionados</li>
					</ul>
				</div>

				<div class="col-sm-12 col-lg-10 col-lg-offset-1">
					<div class="sits-area hidden-xs">
						<div class="sits-anchor">pantalla</div>

						<div class="sits"></div>
					</div>
				</div>

				<div class="col-sm-12 visible-xs">
					<div class="sits-area--mobile">
						<div class="sits-area--mobile-wrap">
							<div class="sits-select">
								<select name="sorting_item" class="sits__sort sit-row"
									tabindex="0">
									<option value="1" selected='selected'>A</option>
									<option value="2">B</option>
									<option value="3">C</option>
									<option value="4">D</option>
									<option value="5">E</option>
									<option value="6">F</option>
									<option value="7">G</option>
									<option value="8">I</option>
									<option value="9">J</option>
									<option value="10">K</option>
									<option value="11">L</option>
								</select> <select name="sorting_item" class="sits__sort sit-number"
									tabindex="1">
									<option value="1" selected='selected'>1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
								</select> <a href="#" class="btn btn-md btn--warning toogle-sits">Choose
									sit</a>
							</div>
						</div>

						<a href="#" class="watchlist add-sits-line">Add new sit</a>

						<aside class="sits__checked">
						<div class="checked-place">
							<span class="choosen-place"></span>
						</div>
						<div class="checked-result">S/.0</div>
						</aside>

						<img alt="" src="images/components/sits_mobile.png">
					</div>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<form id='film-and-time' class="booking-form" method='post' action='reserva.jsp'>
			<input type="hidden" id="txtIDCartelera" name="idCartelera" value="<%= request.getParameter("idCartelera")%>">
			<input type="hidden" id="txtIDSala" name="idSala" />
			<input type="hidden" id="txtFecha" name="fecha">
			<input type='text' class="choosen-number">
			<input type='text' class="choosen-number--cheap"> 
			<input type='text' class="choosen-number--middle">
			<input type='text' class="choosen-number--expansive"> 
			<input type='text' class="choosen-cost"> 
			<input type='text' class="choosen-sits">

			<div class="booking-pagination booking-pagination--margin">
				<button id="btnSiguienteRESERVA" class="booking-pagination__next" type="button"> <span
					class="arrow__text arrow--next">Siguiente</span> <span
					class="arrow__info">Terminar</span>
				</button>
			</div>
		</form>

		<div class="clearfix"></div>
		<div id="test-modal" class="mfp-hide white-popup-block login">
			<p class="login__title">Error<br>
				<span class="login-edition">A.Movie</span></p>
			<p class="success">Debe seleccionar<br> por lo menos una butaca.</p>
		</div>
		<jsp:include page="templates/footer.jsp" flush="true"></jsp:include>
	</div>
	<jsp:include page="templates/librerias.jsp" flush="true"></jsp:include>
	<script type="text/javascript">

		$( document ).ajaxStart(function() {
			$.blockUI({ css: { 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#fff' 
	        } }); 
		});
		$( document ).ajaxStop(function() {
			$.unblockUI();
		});
		$(document).ready(function() {
			init_BookingTwo();
		});
	</script>
</body>
</html>