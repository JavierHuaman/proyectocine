<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="templates/estilos.jsp" flush="true">
	<jsp:param value="Consulta Reserva" name="titulo" />
</jsp:include>
</head>
<body>
	<div class="wrapper">
		<!-- Header section -->
		<jsp:include page="templates/menu.jsp" flush="true"></jsp:include>
		<!-- Search bar -->
        <div class="search-wrapper">
            <div class="container container--add">
                <form id='search-form' method='get' class="search">
                    <input type="text" class="search__field" placeholder="Buscar">
                    <button type='submit' class="btn btn-md btn--danger search__button">Buscar reserva</button>
                </form>
            </div>
        </div>
        
		<!-- Main content -->

        <section class="container">
            <div class="order-container">
                <div class="ticket">
                    <div class="ticket-position">
                        <div class="ticket__indecator indecator--pre"><div class="indecator-text pre--text">online ticket</div> </div>
                        <div class="ticket__inner">

                            <div class="ticket-secondary">
                                <span class="ticket__item">Ticket number <strong class="ticket__number">a126bym4</strong></span>
                                <span class="ticket__item ticket__date">25/10/2013</span>
                                <span class="ticket__item ticket__time">17:45</span>
                                <span class="ticket__item">Cinema: <span class="ticket__cinema">Cineworld</span></span>
                                <span class="ticket__item">Hall: <span class="ticket__hall">Visconti</span></span>
                                <span class="ticket__item ticket__price">price: <strong class="ticket__cost">$60</strong></span>
                            </div>

                            <div class="ticket-primery">
                                <span class="ticket__item ticket__item--primery ticket__film">Film<br><strong class="ticket__movie">The Fifth Estate (2013)</strong></span>
                                <span class="ticket__item ticket__item--primery">Sits: <span class="ticket__place">11F, 12F, 13F</span></span>
                            </div>


                        </div>
                        <div class="ticket__indecator indecator--post"><div class="indecator-text post--text">online ticket</div></div>
                    </div>
                </div>

                <div class="ticket-control">
                    <a href="#" class="watchlist list--download">Download</a>
                    <a href="#" class="watchlist list--print">Print</a>
                </div>

            </div>
        </section>
		
		
		<div class="clearfix"></div>

		<jsp:include page="templates/footer.jsp" flush="true"></jsp:include>
	</div>
	<jsp:include page="templates/librerias.jsp" flush="true"></jsp:include>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.top-scroll').parent().find('.top-scroll').remove();
		});
	</script>
</body>
</html>