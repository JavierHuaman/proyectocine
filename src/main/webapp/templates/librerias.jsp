<jsp:include page="login.jsp" flush="true"></jsp:include>

<!-- JavaScript-->
 <!-- jQuery 1.9.1--> 
 <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
 <!-- Migrate --> 
 <script src="js/external/jquery-migrate-1.2.1.min.js"></script>
 <!-- jQuery UI -->
 <script src="js/jquery-ui.js"></script>
 <!-- Bootstrap 3--> 
 <script src="js/bootstrap.min.js"></script>
 <!-- jQuery REVOLUTION Slider -->
 <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
 <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
 <!-- Swiper slider -->
 <script src="js/external/idangerous.swiper.min.js"></script>

 <!-- Mobile menu -->
 <script src="js/jquery.mobile.menu.js"></script>
  <!-- Select -->
 <script src="js/external/jquery.selectbox-0.2.min.js"></script>
 <!-- Stars rate -->
 <script src="js/external/jquery.raty.js"></script>
 
 <!-- Magnific-popup -->
 <script src="js/external/jquery.magnific-popup.min.js"></script>
 <!-- Form element -->
 <script src="js/external/form-element.js"></script>
 <!-- Form validation -->
 <script src="js/form.js"></script>
 
 <!-- Form validation -->
 <script src="js/jquery-session.js"></script>
 
 <!-- Custom -->
 <script src="js/custom.js"></script>
 <!-- Custom -->
 <script src="js/ui-block.js"></script>