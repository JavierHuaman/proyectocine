<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!-- open/close -->
<div class="overlay overlay-hugeinc">

	<section class="container">

		<div class="col-sm-4 col-sm-offset-4">
			<button type="button" class="overlay-close">Close</button>
			<form id="login-form" class="login" method='get' action="login" novalidate=''>
				<p class="login__title">
					Ingrese <br>
					<span class="login-edition">Bienvenido a AMovie</span>
				</p>
				<div class="field-wrap">
					<p id="errors" class="inv-em alert alert-danger" style="padding: 4px;display: none;"></p>
				</div>
				<div class="field-wrap">
					<input type='email' placeholder='Email' name='email'
						class="login__input">

				</div>
				<div class="field-wrap">
				 <input type='password'
						placeholder='Password' name='password' class="login__input">
				</div>
				<div class="field-wrap">
				
					<label for='#informed' class='login__check-info'>&nbsp;</label>
				</div>
				
				<div class="login__control">
					<button type='submit' class="btn btn-md btn--warning btn--wider">Ingresar</button>
					<a href="#" class="login__tracker form__tracker" id="btn-register">No esta registrado?</a>
				</div>
			</form>
			<form id="register-form" class="login" style="display:none;" action="cliente" method="post">
				<p class="login__title">
					Registrarse <br>
					<span class="login-edition">Bienvenido a AMovie</span>
				</p>
				<div class="field-wrap">
					<input type="hidden"  name="operacion" value="registrar"/>
					<input class="login__input" type="text" placeholder="DNI" name="dni" maxlength="8"/>
					<input class="login__input" type="text" placeholder="Nombre" name="nombre"/>
					<input class="login__input" type="text" placeholder="Apellido Paterno" name="apePaterno"/>
					<input class="login__input" type="text" placeholder="Apellido Materno" name="apeMaterno"/>
					<input class="login__input" type="text" placeholder="Celular" name="celular" maxlength="9"/>
					<input class="login__input" type="email" placeholder="Correo" name="email"/>
					<input class="login__input" type="password" placeholder="Password"  name="password"/>
				</div>
				<div class="login__control">
					<button type='submit' class="btn btn-md btn--warning btn--wider">Registrarse</button>
					<a href="#" class="login__tracker form__tracker" id="btn-login">Iniciar sesion?</a>
				</div>
			</form>
		</div>

	</section>
</div>
