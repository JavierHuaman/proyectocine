<%@page import="bean.Cliente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!-- Header section -->
<header class="header-wrapper">
	<div class="container">
		<!-- Logo link-->
		<a href='index.jsp' class="logo"> <img alt='logo'
			src="images/logo.png">
		</a>

		<!-- Main website navigation-->
		<nav id="navigation-box">
			<!-- Toggle for mobile menu mode -->
			<a href="#" id="navigation-toggle"> <span class="menu-icon">
					<span class="icon-toggle" role="button"
					aria-label="Toggle Navigation"> <span class="lines"></span>
				</span>
			</span>
			</a>

			<!-- Link navigation -->
			<ul id="navigation">
				<li><span class="sub-nav-toggle plus"></span> <a
					href="cartelera.jsp">Cartelera</a></li>
			</ul>
		</nav>

		<!-- Additional header buttons / Auth and direct link to booking-->
		<div class="control-panel">
			<% if (session.getAttribute("login")!=null){  
				 Cliente c = (Cliente) session.getAttribute("login");
			%>
			<div class="auth auth--home">
				<div class="auth__show">
					<span class="auth__image"> <img alt=""
						src="http://placehold.it/31x31">
					</span>
				</div>
				<a href="#" class="btn btn--sign btn--singin"> <%=c.getNombres() %> </a>
				<ul class="auth__function">
					<li><a href="profile.jsp" class="auth__function-item">Perfil</a></li>
					<li><a href="listaReservas.jsp" class="auth__function-item">Reservas</a></li>
					<li><a href="logout" class="auth__function-item">Salir</a></li>
				</ul>
			</div>
			<%} else{ %>
			<a href="#" class="btn btn--sign login-window">Log in</a>
			<%} %>
		</div>

	</div>
</header>
