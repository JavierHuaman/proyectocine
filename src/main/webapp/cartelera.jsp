<%@page import="bean.Cliente"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="templates/estilos.jsp" flush="true">
	<jsp:param value="Cartelera" name="titulo" />
</jsp:include>
</head>
<body>
	<%
		Cliente bean = (Cliente) session.getAttribute("login");
		int flag = 0;
		if(bean != null){
			flag = 1;
		}
	%>
	<div class="wrapper">
		<!-- Header section -->
		<jsp:include page="templates/menu.jsp" flush="true"></jsp:include>
		<!-- Main content -->
		<section class="container">
		<input type="hidden" value="<%=flag %>" id="txtSessionValue" />
		<div class="col-sm-12">
			<h2 class="page-heading">Peliculas en cartelera</h2>

			<div class="select-area">
				<form class="select" method='get'>
					<select name="select_item" class="select__sort" id="listaCiudad"
						tabindex="0">
						<option value="--Seleccione--">Seleccione</option>
					</select>
				</form>

				<form class="select select--cinema" method='get' id="boxComplejo">
					<select name="select_item" class="select__sort" tabindex="0"
						id="listaComplejo">
						<option value="--Seleccione--">Seleccione</option>
					</select>
				</form>

				<div class="datepicker">
					<span class="datepicker__marker"><i class="fa fa-calendar"></i>Fecha</span>
					<input type="text" id="datepicker"
						value='<%=new SimpleDateFormat("yyyy/MM/dd").format(new Date())%>'
						class="datepicker__input">
				</div>
				<button class="btn btn-md btn--warning btn--wider" id="idBuscar"
					style="float: right;">BUSCAR</button>
			</div>

			<div id="Response"></div>


		</div>

		</section>

		<div class="clearfix"></div>
		<div id="test-modal" class="mfp-hide white-popup-block login">
			<p class="login__title">Error<br>
				<span class="login-edition">A.Movie</span></p>
			<p class="success">Debe seleccionar<br> un horario.</p>
		</div>
		<jsp:include page="templates/footer.jsp" flush="true"></jsp:include>
	</div>
	<jsp:include page="templates/librerias.jsp" flush="true"></jsp:include>
	<script type="text/javascript">
		$(document).ajaxStart(function() {
			$.blockUI({
				css : {
					border : 'none',
					padding : '15px',
					backgroundColor : '#000',
					'-webkit-border-radius' : '10px',
					'-moz-border-radius' : '10px',
					opacity : .5,
					color : '#fff'
				}
			});
		});
		$(document).ajaxStop(function() {
			$.unblockUI();
		});
		$(document).ready(function() {
			init_MovieList();
		});
	</script>
</body>
</html>